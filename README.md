# README #

This README will help you install and use libCCNetworkHelper.jar a Java Library I developed to improve checking network connection on Android Devices as well as handling the network types of android devices. I clean up and remove plenty of code into just a couple lines for you to use. 

Code from this Java Library is still being developed, but has been tested with the current Android studio version 2.3.2. 

### This library implements ###
* ConnectivityManager
* NetworkInfo
* checkSelfPermission


### How to install Java Library ###

* Press on the Downloads button on the left side of this page and download the repository. 

![Screen Shot 2017-06-13 at 22.47.08.png](https://bitbucket.org/repo/kMkzy6M/images/3277785286-Screen%20Shot%202017-06-13%20at%2022.47.08.png)

* Then press on file while inside Android Studio and select new module.

![Screen Shot 2017-06-13 at 23.13.10.png](https://bitbucket.org/repo/kMkzy6M/images/3982279084-Screen%20Shot%202017-06-13%20at%2023.13.10.png)

* Select Import .JAR/AAR Package and press next.

![Screen Shot 2017-06-13 at 23.13.22.png](https://bitbucket.org/repo/kMkzy6M/images/215187201-Screen%20Shot%202017-06-13%20at%2023.13.22.png)

* Choose the location of the Java Library, press on the button with 3 dots on the right and locate the java library. It should be in your downloads. 

![Screen Shot 2017-06-13 at 23.14.26.png](https://bitbucket.org/repo/kMkzy6M/images/780527460-Screen%20Shot%202017-06-13%20at%2023.14.26.png)

And that's it you should be all setup.

### Library Documentation ###
* Import the library into your file with 
* import com.caridad.creations.ccnetworkhelper.libCCNetworkHelper;


### Class ###
* Call static methods with libCCNetworkHelper class. 

### Methods ###
* returnNetworkType - takes in a context and will return the network type the device is using in a string form. (TYPE_WIFI, TYPE_MOBILE, TYPE_VPN, TYPE_BLUETOOTH)

* connectToNetworkType - takes in a context and returns a Boolean if the device network type chosen exists.

* connectToNetwork - takes in a context and checks if the device has any type of network connection.
 
### returnNetworkType ###
* Log.i("Device network type ", libCCNetworkHelper.returnNetworkType(this));

### connectToNetworkType ###
* if (libCCNetworkHelper.connectToNetworkType(this).TYPE_WIFI) {
            // Pull large data
 } else if (libCCNetworkHelper.connectToNetworkType(this).TYPE_MOBILE) { 
            // Pull normal data and wait for wifi connection
 }

### connectToNetwork ###
* if(libCCNetworkHelper.connectToNetwork(this)) {
// device has network connection
}else {
// device has no network connection
}

### Official Documentation ###
* [ConnectivityManager](https://developer.android.com/reference/android/net/ConnectivityManager.html)

* [NetworkInfo](https://developer.android.com/reference/android/net/NetworkInfo.html)

* [checkSelfPermission](https://developer.android.com/reference/android/support/v4/content/ContextCompat.html)